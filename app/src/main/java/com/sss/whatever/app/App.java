package com.sss.whatever.app;

import com.ballastlane.android.baselibrary.app.BaseApp;
import com.ballastlane.android.baselibrary.app.modules.network.AuthenticationInterceptor;
import com.ballastlane.android.baselibrary.app.modules.network.NetworkModule;
import com.sss.whatever.app.di.ApiServicesModule;
import com.sss.whatever.app.di.DaggerWhateverComponent;
import com.sss.whatever.app.di.WhateverComponent;
import com.sss.whatever.utils.PreferencesUtils;

import timber.log.Timber;

/**
 * @author Daniela Perez danielaperez@kogimobile.com on 4/20/18.
 */

public class App extends BaseApp {

    public static WhateverComponent texasamComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        texasamComponent = DaggerWhateverComponent.builder()
                .networkModule(new NetworkModule(new AuthenticationInterceptor()))
                .appModule(appModule)
                .apiServicesModule(new ApiServicesModule())
                .build();

        iniTimber();
        initPreferences();
    }

    private void initPreferences() {
        PreferencesUtils.init(this);
    }

    private void iniTimber() {
        Timber.plant(
                new Timber.DebugTree() {
                    // Add the line number to the tag.
                    @Override
                    protected String createStackElementTag(StackTraceElement element) {
                        return super.createStackElementTag(element) + ':' + element.getLineNumber();
                    }
                });
    }
}

