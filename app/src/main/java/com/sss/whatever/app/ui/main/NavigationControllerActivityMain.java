package com.sss.whatever.app.ui.main;

import com.ballastlane.android.baselibrary.common.base.BaseActivityInnerNavigation;
import com.ballastlane.android.baselibrary.common.base.BaseFragment;
import com.ballastlane.android.baselibrary.common.base.navigation.BaseActivityInnerNavigationController;
import com.sss.whatever.R;

import java.util.HashMap;


public class NavigationControllerActivityMain extends BaseActivityInnerNavigationController {

    private HashMap<String, BaseFragment> navFragments;

    public NavigationControllerActivityMain(BaseActivityInnerNavigation activity) {
        super(activity, R.id.container);
        initFragments();
    }

    private void initFragments() {
        this.navFragments = new HashMap<>();
        this.navFragments.put(getSection1Title(), FrgCompanies.newInstance());
        this.navFragments.put(getSection2Title(), FrgAccount.newInstance());
    }

    private String getSection1Title() {
        return getContext().getString(R.string.nav_list);
    }

    private String getSection2Title() {
        return getContext().getString(R.string.nav_account);
    }


    public void navigateToSection1() {
        navigateToRootLevel(this.navFragments.get(getSection1Title()), getSection1Title());
    }

    public void navigateToSection2() {
        navigateToRootLevel(this.navFragments.get(getSection2Title()), getSection2Title());
    }

}
