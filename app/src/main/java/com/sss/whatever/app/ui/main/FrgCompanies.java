package com.sss.whatever.app.ui.main;

import android.arch.lifecycle.ViewModelProviders;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ballastlane.android.baselibrary.common.base.busevents.alert.EventAlertDialog;
import com.ballastlane.android.baselibrary.common.base.recyclerview.BaseFragmentRecyclerView;
import com.sss.whatever.R;
import com.sss.whatever.databinding.FrgCompaniesBinding;

import org.greenrobot.eventbus.EventBus;
import org.jetbrains.annotations.NotNull;

import java.util.List;

import io.reactivex.Single;
import io.reactivex.functions.Consumer;

public class FrgCompanies extends BaseFragmentRecyclerView<RepositoryCompanies> {

    private FrgCompaniesBinding binding;
    private ViewModelCompanies viewModel;
    private AdapterCompanies adapter = new AdapterCompanies();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.viewModel = ViewModelProviders.of(this).get(ViewModelCompanies.class);
        doLoadItems();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.binding = DataBindingUtil.inflate(inflater, R.layout.frg_companies, container, false);
        return this.binding.getRoot();
    }

    @Override
    protected void initVars() {
        setAdapter(adapter);
        setLoadEnabled(true);

    }

    @Override
    protected void initViews() {
        getRecyclerView().setLayoutManager(new LinearLayoutManager(getActivity()));
        getRecyclerView().setHasFixedSize(true);
    }

    @Override
    protected void initListeners() {
        this.binding.swipeCompanies.setOnRefreshListener(
                new SwipeRefreshLayout.OnRefreshListener() {
                    @Override
                    public void onRefresh() {
                        doRefreshItems();
                    }
                }
        );

        adapter.setListener(new AdapterCompanies.CompaniesListener() {
            @Override
            public void onClick(RepositoryCompanies companie) {
                ActivityPay.start(getActivity(), new Bundle());
            }

            @Override
            public void onLongClick(RepositoryCompanies companie) {
                EventBus.getDefault().post(EventAlertDialog
                        .Companion.getBuilder()
                        .withTitle(getString(R.string.delete_companie_title))
                        .withMessage(getString(R.string.delete_companie_msg, companie.getName()))
                        .withPositiveButton(getString(R.string.positive_option), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                //TODO: Llamar a servicio de eliminar compania
                            }
                        })
                        .withNegativeButton(getString(R.string.negative_option), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        })
                        .build());
            }
        });
    }

    public static FrgCompanies newInstance() {

        Bundle args = new Bundle();
        FrgCompanies fragment = new FrgCompanies();
        fragment.setArguments(args);
        return fragment;
    }

    @org.jetbrains.annotations.Nullable
    @Override
    protected RecyclerView getRecyclerView() {
        return binding.recyclerCompanies;
    }

    @NotNull
    @Override
    protected Single<Pair<List<RepositoryCompanies>, Boolean>> getItemsFromSource() {
        return viewModel.getListItems();
    }

    @NotNull
    @Override
    protected Single<Pair<List<RepositoryCompanies>, Boolean>> getMoreItemsFromSource() {
        return viewModel.getMoreItems();
    }

    @NonNull
    @Override
    protected Consumer<Throwable> onRefreshItemsLoadFail() {
        hideRefresh();
        return super.onRefreshItemsLoadFail();
    }

    @NonNull
    @Override
    protected Consumer<Pair<List<RepositoryCompanies>, Boolean>> onRefreshItemsLoadSuccess() {
        hideRefresh();
        return super.onRefreshItemsLoadSuccess();
    }

    private void hideRefresh() {
        this.binding.swipeCompanies.setRefreshing(false);
    }
}
