package com.sss.whatever.app.ui.main;

import android.arch.lifecycle.ViewModel;
import android.support.annotation.NonNull;
import android.util.Pair;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.SingleObserver;
import io.reactivex.SingleSource;
import io.reactivex.functions.Function;


/**
 * @author Daniela Perez danielaperez@kogimobile.com on 4/19/18.
 */
public class ViewModelCompanies extends ViewModel {

    public Single<List<RepositoryCompanies>> sessions() {
//        return Repository.getSessionRepository().sessions();
        List<RepositoryCompanies> companies = new ArrayList<>();
        companies.add(new RepositoryCompanies("Company 1"));
        companies.add(new RepositoryCompanies("Company 2"));
        companies.add(new RepositoryCompanies("Company 3"));
        companies.add(new RepositoryCompanies("Company 4"));
        companies.add(new RepositoryCompanies("Company 5"));
        companies.add(new RepositoryCompanies("Company 6"));
        companies.add(new RepositoryCompanies("Company 7"));
       return Single.just(companies);
    }

    @NonNull
    private Single<Pair<List<RepositoryCompanies>, Boolean>> getMapperFunc(final boolean hasMoreToload) {

        return sessions().flatMap(new Function<List<RepositoryCompanies>, SingleSource<? extends Pair<List<RepositoryCompanies>, Boolean>>>() {
                                      @Override
                                      public SingleSource<? extends Pair<List<RepositoryCompanies>, Boolean>> apply(List<RepositoryCompanies> repositorySessions) throws Exception {
                                          return Single.just(new Pair<>(
                                                  repositorySessions,
                                                  hasMoreToload));
                                      }
                                  }
        );
    }

    public Single<Pair<List<RepositoryCompanies>, Boolean>> getListItems() {
        return getMapperFunc(false);

    }

    public Single<Pair<List<RepositoryCompanies>, Boolean>> getMoreItems() {
        return getMapperFunc(true);
    }
}
