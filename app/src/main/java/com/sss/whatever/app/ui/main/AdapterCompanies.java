package com.sss.whatever.app.ui.main;

import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ballastlane.android.baselibrary.common.base.recyclerview.BaseAdapter;
import com.sss.whatever.R;
import com.sss.whatever.databinding.ListItemCompaniesBinding;

import timber.log.Timber;

/**
 * Created by Mariangela Salcedo (mariangelasalcedo@ballastlane.com) on 5/8/18.
 * Copyright (c) 2018 Ballast Lane Applications LLC. All rights reserved.
 */
public class AdapterCompanies extends BaseAdapter<RepositoryCompanies, BaseAdapter.BaseViewHolder<RepositoryCompanies>> {

    CompaniesListener listener;

    public static final int EMPTY_VIEW = 2000;
    public static final int LOADING_VIEW = 4000;
    public static final int LOAD_MORE_VIEW = 5000;

    @NonNull
    @Override
    public BaseViewHolder<RepositoryCompanies> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ViewDataBinding binding;
        switch (viewType) {
            case EMPTY_VIEW: {
                binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.list_item_companies_empty, parent, false);
                return new EmptyViewHolder<>(binding.getRoot());
            }
            case LOAD_MORE_VIEW: {
                binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.list_item_companies_loading_more, parent, false);
                return new EmptyViewHolder<>(binding.getRoot());
            }
            case LOADING_VIEW: {
                binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.list_item_companies_loading, parent, false);
                return new EmptyViewHolder<>(binding.getRoot());
            }

            default:
                binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()), R.layout.list_item_companies, parent, false);
                return new SessionHolder(binding.getRoot());
        }
    }


    private class SessionHolder extends BaseViewHolder<RepositoryCompanies>
            implements View.OnClickListener, View.OnLongClickListener {

        public SessionHolder(View view) {
            super(view);
            view.setOnClickListener(this);
            view.setOnLongClickListener(this);
        }

        @Override
        protected void bindView(RepositoryCompanies repositorySession) {
            getBinding().tvTitle.setText(repositorySession.getName());

        }

        @Override
        protected ListItemCompaniesBinding getBinding() {
            return super.getBinding();
        }

        @Override
        public void onClick(View view) {
            listener.onClick(getItem(getAdapterPosition()));
        }

        @Override
        public boolean onLongClick(View view) {
             listener.onLongClick(getItem(getAdapterPosition()));
            return true;
        }
    }

    public CompaniesListener getListener() {
        return listener;
    }

    public void setListener(CompaniesListener listener) {
        this.listener = listener;
    }

    public interface CompaniesListener {
        void onClick(RepositoryCompanies companie);

        void onLongClick(RepositoryCompanies companie);

    }
}
