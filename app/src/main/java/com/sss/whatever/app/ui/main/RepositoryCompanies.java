package com.sss.whatever.app.ui.main;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RepositoryCompanies {

    @Expose
    @SerializedName("name")
    private String name;

    public RepositoryCompanies(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
