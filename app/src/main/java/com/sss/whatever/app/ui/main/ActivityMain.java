package com.sss.whatever.app.ui.main;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.view.MenuItem;

import com.ballastlane.android.baselibrary.common.base.BaseActivityInnerNavigation;
import com.sss.whatever.R;
import com.sss.whatever.app.ui.main.viewmodel.ViewModelActivityMain;
import com.sss.whatever.databinding.ActivityMainBinding;

public class ActivityMain extends BaseActivityInnerNavigation {

    private ActivityMainBinding binding;
    private ViewModelActivityMain viewModel;


    public NavigationControllerActivityMain getNavigationControllerMain() {
        return (NavigationControllerActivityMain) super.getNavigationController();
    }

    @Override
    protected void initVars() {
        setNavigationController(new NavigationControllerActivityMain(this));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.viewModel = ViewModelProviders.of(this).get(ViewModelActivityMain.class);
        this.binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
    }

    @Override
    protected void initViews() {
        initToolbar();
        navigateToFirstFragment();
        binding.bottombar.setSelectedItemId(R.id.menu_list);
    }

    @Override
    protected void initListeners() {
        binding.bottombar.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.menu_list:
                        navigateToFirstFragment();
                        return true;
                    case R.id.menu_account:
                        navigateToSecondFragment();
                        return true;
                }
                return false;
            }
        });
    }

    private void initToolbar() {
        setSupportActionBar(binding.includeToolbar.toolbar);
        getSupportActionBar().setTitle(getString(R.string.app_name));
    }


    private void navigateToFirstFragment() {
        getNavigationControllerMain().navigateToSection1();
    }

    private void navigateToSecondFragment() {
        getNavigationControllerMain().navigateToSection2();
    }


}
