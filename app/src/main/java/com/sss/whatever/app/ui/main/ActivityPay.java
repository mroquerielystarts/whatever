package com.sss.whatever.app.ui.main;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.view.MenuItem;

import com.ballastlane.android.baselibrary.common.base.BaseActivity;
import com.ballastlane.android.baselibrary.common.base.BaseActivityInnerNavigation;
import com.sss.whatever.R;
import com.sss.whatever.app.ui.main.viewmodel.ViewModelActivityMain;
import com.sss.whatever.databinding.ActivityMainBinding;
import com.sss.whatever.databinding.ActivityPayBinding;

public class ActivityPay extends BaseActivity {

    private ActivityPayBinding binding;
    private ViewModelActivityPay viewModel;

    public static void start(Context context, Bundle bundle) {
        Intent intent = new Intent(context, ActivityPay.class);
        intent.putExtras(bundle);
        context.startActivity(intent);
    }

    @Override
    protected void initVars() {
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.viewModel = ViewModelProviders.of(this).get(ViewModelActivityPay.class);
        this.binding = DataBindingUtil.setContentView(this, R.layout.activity_pay);
    }

    @Override
    protected void initViews() {
        initToolbar();
    }

    @Override
    protected void initListeners() {

    }

    private void initToolbar() {
        setSupportActionBar(binding.includeToolbar.toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }


}
