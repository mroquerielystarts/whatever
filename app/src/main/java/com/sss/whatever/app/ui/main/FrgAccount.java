package com.sss.whatever.app.ui.main;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ballastlane.android.baselibrary.common.base.BaseFragment;
import com.sss.whatever.R;
import com.sss.whatever.databinding.FrgAccountBinding;
import com.sss.whatever.databinding.FrgCompaniesBinding;

public class FrgAccount extends BaseFragment{


    private FrgAccountBinding binding;
    private ViewModelAccount viewModel;



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.viewModel = ViewModelProviders.of(this).get(ViewModelAccount.class);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.binding = DataBindingUtil.inflate(inflater, R.layout.frg_account, container, false);
        return this.binding.getRoot();
    }

    @Override
    protected void initVars() {

    }

    @Override
    protected void initViews() {

    }

    @Override
    protected void initListeners() {

    }

    public static FrgAccount newInstance() {

        Bundle args = new Bundle();
        FrgAccount fragment = new FrgAccount();
        fragment.setArguments(args);
        return fragment;
    }
}
